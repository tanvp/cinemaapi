﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaAPI.Models;
using Newtonsoft.Json;

namespace CinemaAPI.Controllers
{
    public class MoviesController : ApiController
    {
        private CinemaDBEntities db = new CinemaDBEntities();

        // GET: api/Movies
        [ResponseType(typeof(Movie))]
        public IHttpActionResult GetMovies()
        {
            List<MovieModel> list = new List<MovieModel>();
            MovieModel m = new MovieModel();
            //foreach (var mo in db.Movies)
            //{
            //    m.MovieID = mo.MovieID;
            //    m.MovieName = mo.MovieName;
            //    m.ImageUrl = mo.ImageUrl;
            //    m.Length = mo.Length;
            //    m.OpeningDate = mo.OpeningDate.GetValueOrDefault().ToShortDateString();
            //    m.Formats = mo.Formats.Select(mov => new FormatModel
            //    {
            //        FormatID = mov.FormatID,
            //        FormatName = mov.FormatName,
            //    }).ToList();
            //    list.Add(m);
            //}
            var movieList = db.Movies.Select(movie => new MovieModel
            {
                MovieID = movie.MovieID,
                MovieName= movie.MovieName,
                ImageUrl = movie.ImageUrl,
                Length = movie.Length,
                OpeningDate = movie.OpeningDate.ToString(),
                Formats = movie.Formats.Select(mov => new FormatModel
                {
                    FormatID = mov.FormatID,
                    FormatName = mov.FormatName,
                }).ToList()

        });

            String movieJson = JsonConvert.SerializeObject(movieList);
            if (db.Movies == null)
            {
                return NotFound();
            }

            return Json(
                new
                {
                    movieJson
                });
        }

        // GET: api/Movies/5
        [ResponseType(typeof(Movie))]
        public IHttpActionResult GetMovie(int id)
        {
            var movie = db.Movies.Where(mooo=>mooo.MovieID==id);

            List<MovieModel> list = new List<MovieModel>();
            MovieModel m = new MovieModel();
            foreach (var mo in movie)
            {
                m.MovieID = mo.MovieID;
                m.MovieName = mo.MovieName;
                m.ImageUrl = mo.ImageUrl;
                m.Length = mo.Length;
                m.Formats=mo.Formats.Select(mov => new FormatModel
                {
                    FormatID=mov.FormatID,
                    FormatName=mov.FormatName,
                }).ToList();
                list.Add(m);
            }

            String movieJson= JsonConvert.SerializeObject(list);
            if (movie == null)
            {
                return NotFound();
            }

            return Json(
                new
                {
                    movieJson
                });
        }



        [ResponseType(typeof(Movie))]
        [HttpPost]
        public IHttpActionResult GetMoviesActiveNowShowPost()
        {
            HttpContent requestContent = Request.Content;
            string nowStartTime = requestContent.ReadAsStringAsync().Result;
            SendModel sendModel = JsonConvert.DeserializeObject<SendModel>(nowStartTime);
            DateTime startTime = DateTime.Parse(sendModel.nowStartTime);
            var moviesSelected = db.Movies.Where(mooo => mooo.Shows.Any(s => s.StartTime <= startTime && s.EndTime>= startTime) && mooo.Active == true);
            HashSet<int> formatMovieIdSelected= new HashSet<int>();
            List<MovieViewModel> listMovie = new List<MovieViewModel>();
            foreach (var movie in moviesSelected)
            {
                var shows = db.Shows.Where(s => s.Active == true
                                                         && s.MovieID == movie.MovieID);
                formatMovieIdSelected = new HashSet<int>();
                foreach (var show in shows)
                {
                    //add format is filter by show and movied id 
                    formatMovieIdSelected.Add(show.Format.FormatID);
                }
                MovieViewModel movieModel = new MovieViewModel()
                {
                    MovieID = movie.MovieID,
                    MovieName = movie.MovieName,
                    //get all format is contained by formatMovieIdSelected
                    Formats = movie.Formats.Where(f => formatMovieIdSelected.Any(fm => fm == f.FormatID)).Select(f => new FormatModel()
                    {
                        FormatID = f.FormatID,
                        FormatName = f.FormatName
                    }).ToList(),
                };
                listMovie.Add(movieModel);
            }
           
            String movieJson = JsonConvert.SerializeObject(listMovie);
            if (moviesSelected == null)
            {
                return NotFound();
            }

            return Json(
                new
                {
                    movieJson
                });
        }


        [ResponseType(typeof(Movie))]
        [HttpPost]
        [Route("api/Movies/GetMoviesActiveNowShow2")]
        public IHttpActionResult GetMoviesActiveNowShow2Post()
        {
            HttpContent requestContent = Request.Content;
            string nowStartTime = requestContent.ReadAsStringAsync().Result;
            SendModel sendModel = JsonConvert.DeserializeObject<SendModel>(nowStartTime);
            DateTime startTime = DateTime.Parse(sendModel.nowStartTime);
            List<Movie> moviesSelected = new List<Movie>();
            var movieA = db.Movies.ToList();
            foreach (var movie in movieA)
            {
                var show = movie.Shows;
                if (show.Any(s => s.StartTime > startTime)) {
                    moviesSelected.Add(movie);
                }
            }

              //  var moviesSelected = db.Movies.Where(mooo => mooo.Shows.Any(s => s.StartTime <= startTime) && mooo.Active == true);
          
            HashSet<int> formatMovieIdSelected = new HashSet<int>();
            List<MovieModel> listMovie = new List<MovieModel>();
            foreach (var movie in moviesSelected)
            {
                var shows = db.Shows.Where(s => s.Active == true
                                                         && s.MovieID == movie.MovieID);
                formatMovieIdSelected = new HashSet<int>();
                foreach (var show in shows)
                {
                    //add format is filter by show and movied id 
                    formatMovieIdSelected.Add(show.Format.FormatID);
                }
                MovieModel movieModel = new MovieModel()
                {
                    MovieID = movie.MovieID,
                    MovieName = movie.MovieName,
                    ImageUrl = movie.ImageUrl,
                    Length = movie.Length,
                    OpeningDate = movie.OpeningDate.ToString(),
                    //get all format is contained by formatMovieIdSelected
                    Formats = movie.Formats.Where(f => formatMovieIdSelected.Any(fm => fm == f.FormatID)).Select(f => new FormatModel()
                    {
                        FormatID = f.FormatID,
                        FormatName = f.FormatName
                    }).ToList(),
                };
                listMovie.Add(movieModel);
            }

            String movieJson = JsonConvert.SerializeObject(listMovie);
            if (moviesSelected == null)
            {
                return NotFound();
            }

            return Json(
                new
                {
                    movieJson
                });
        }
        public class FormatModel
        {
            public int FormatID { get; set; }
            public string FormatName { get; set; }
        }

        public class SendModel {
            public string nowStartTime { get; set; }
        }

        public class MovieViewModel{
            public int MovieID { get; set; }
            public string MovieName { get; set; }
            public virtual ICollection<FormatModel> Formats { get; set; }

        }

        public class MovieModel{
            public int MovieID { get; set; }
            public string MovieName { get; set; }
            public string ImageUrl { get; set; }
            public int Length { get; set; }
            public String OpeningDate { get; set; }
            public virtual ICollection<FormatModel> Formats { get; set; }
        }

        public class GenreModel
        {
            public int GenreID { get; set; }
            public string GenreName { get; set; }
            public bool Active { get; set; }
        }

        // PUT: api/Movies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMovie(int id, Movie movie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != movie.MovieID)
            {
                return BadRequest();
            }

            db.Entry(movie).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Movies/5
        [ResponseType(typeof(Movie))]
        public IHttpActionResult DeleteMovie(int id)
        {
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return NotFound();
            }

            db.Movies.Remove(movie);
            db.SaveChanges();

            return Ok(movie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MovieExists(int id)
        {
            return db.Movies.Count(e => e.MovieID == id) > 0;
        }
    }
}