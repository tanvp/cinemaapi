﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaAPI.Models;
using Newtonsoft.Json;

namespace CinemaAPI.Controllers
{
    public class ShowsController : ApiController
    {
        private CinemaDBEntities db = new CinemaDBEntities();

        // GET: api/Shows
        public IQueryable<Show> GetShows()
        {
            return db.Shows;
        }

        // GET: api/Shows/5
        [ResponseType(typeof(Show))]
        public IHttpActionResult GetShow(int id)
        {
            Show show = db.Shows.Find(id);
            if (show == null)
            {
                return NotFound();
            }

            return Ok(show);
        }

        [ResponseType(typeof(Show))]
        [HttpGet]
        [Route("api/shows/GetShowsActiveByMovieId/{movieId}/{formatId}")]
        public IHttpActionResult getShowsByMovieAndFormatId(int movieId, int formatId)
        {
            var showsSelected = db.Shows.Where(s => s.Active && s.MovieID == movieId && s.TechniqueID == formatId);                                                            
            if (showsSelected == null)
            {
                return NotFound();
            }
            List<ShowModel> listShowsSelected = new List<ShowModel>();
            foreach (var s in showsSelected)
            {
                ShowModel showSelected = new ShowModel();
                showSelected.ShowID = s.ShowID;
                showSelected.Time = s.Time.Value.ToShortDateString();
                showSelected.StartTime = s.StartTime.Value.ToShortTimeString();
                showSelected.EndTime = s.EndTime.Value.ToShortTimeString();
                showSelected.MovieID = s.MovieID;
                showSelected.FormatID = s.Format.FormatID;
                if (showSelected != null)
                {
                    listShowsSelected.Add(showSelected);
                }
            }
            String showsSelectedJson = JsonConvert.SerializeObject(listShowsSelected);
            return Json(new
            {
                showsSelectedJson,
            });
        }

        // GET: api/Movies/5
        [ResponseType(typeof(Show))]
        [HttpGet]
        [Route("api/shows/GetShowTicketsByShowId/{showId}")]
        public IHttpActionResult GetShowTicketsByShowId(int showId)
        {
            var showSelected = db.Shows.Where(s => s.ShowID == showId).FirstOrDefault();

            List<ShowViewModel> list = new List<ShowViewModel>();
            ShowViewModel show = new ShowViewModel()
            {
                ShowID = showSelected.ShowID,
                StartTime = showSelected.StartTime.Value.ToShortTimeString(),
                EndTime = showSelected.EndTime.Value.ToShortTimeString(),
                Time = showSelected.Time.Value.ToShortDateString(),
                RoomName = showSelected.Room.RoomName,
                MovieAndFormatName = showSelected.Movie.MovieName + " (" + showSelected.Format.FormatName + ") ",
                Tickets = showSelected.Tickets.Where(t=>t.TicketStatus==true).Select(t => new TicketModel()
                {
                    SeatID = t.SeatID,
                    SeatName = t.Seat.Position,
                    TicketCode = t.TicketCode == null ? "" : t.TicketCode,
                    Price = (Double)t.Price,
                    SeatTypeName= t.Seat.SeatType.SeatType1,
                }).ToList()
            };
            list.Add(show);
            String showTicketsJson = JsonConvert.SerializeObject(list);
            return Json(new {
                showTicketsJson
            });
            }


        public class ShowModel
        {
            public int ShowID { get; set; }
            public int FormatID { get; set; }
            public int MovieID {get; set; }
            public String Time { get; set; }
            public String StartTime { get; set; }
            public String EndTime { get; set; }
            public bool Active { get; set; }
        }

        public class ShowViewModel {
            public int ShowID { get; set; }
            public String RoomName { get; set; }
            public String Time { get; set; }
            public String MovieAndFormatName { get; set; }

            public String StartTime { get; set; }
            public String EndTime { get; set; }
            public virtual ICollection<TicketModel> Tickets { get; set; }
        }
        public class TicketModel
        {
            public int SeatID { get; set; }
            public string TicketCode { get; set; }
            public Nullable<Double> Price { get; set; }
            public String SeatName { get; set; }
            public String SeatTypeName { get; set; }
        }
    }
}