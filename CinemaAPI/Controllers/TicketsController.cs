﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaAPI.Models;
using Newtonsoft.Json;

namespace CinemaAPI.Controllers
{
    public class TicketsController : ApiController
    {
        private CinemaDBEntities db = new CinemaDBEntities();

        // GET: api/Tickets
        public IQueryable<Ticket> GetTickets()
        {
            return db.Tickets;
        }

        // GET: api/Tickets/5
        [ResponseType(typeof(Ticket))]
        public IHttpActionResult GetTicket(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return NotFound();
            }

            return Ok(ticket);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketExists(int id)
        {
            return db.Tickets.Count(e => e.ShowID == id) > 0;
        }
        [HttpPost]
        [Route("api/Tickets/bookTicket")]
        public IHttpActionResult bookTicketPost()
        {
            HttpContent requestContent = Request.Content;
            String listTicketId = requestContent.ReadAsStringAsync().Result;
            var jsonResult = JsonConvert.DeserializeObject<JsonString>(listTicketId);
            var listTicket = JsonConvert.DeserializeObject<ReceiveModel>(jsonResult.jsonResult);
            bool isSuccess;
            String generatedString = "";
            if (listTicket != null)
            {
                try
                {
                    generatedString = RandomString(10);
                    //update ticket
                    foreach (var seatId in listTicket.seatIdList)
                    {
                        var ticketSelected = db.Tickets.Where(t => t.SeatID == seatId && t.ShowID == listTicket.showId).FirstOrDefault();
                        ticketSelected.TicketStatus = false;
                        ticketSelected.OrderID = generatedString;
                        ticketSelected.Username = listTicket.username;
                    }
                    Decimal totalPrice = Decimal.Parse(listTicket.totalPrice.ToString());
                    db.BalanceTransactions.Add(new BalanceTransaction
                    {
                        User = listTicket.username,
                        CreatedDate = System.DateTime.Now,
                        TransactionType = 2,
                        Amount = totalPrice,
                        OrderId = generatedString
                    });
                    var accountSelected = db.Accounts.Where(a => a.Username == listTicket.username).FirstOrDefault();
                    accountSelected.Balance = accountSelected.Balance - (decimal)listTicket.totalPrice;
                    db.SaveChanges();
                    isSuccess = true;
                }

                //list integer searID, double total Price
                catch (Exception e)
                {
                    isSuccess = false;
                }
            }
            else
                isSuccess = false;
            return Json(new { isSuccess, generatedString });
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public class ReceiveModel
        {
            public string username { get; set; }
            public int showId { get; set; }
            public List<int> seatIdList { get; set; }
            public Double totalPrice { get; set; }
        }
        public class JsonString
        {
            public string jsonResult { get; set; }
        }
    }
}