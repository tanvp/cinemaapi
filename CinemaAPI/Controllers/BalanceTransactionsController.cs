﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaAPI.Models;
using Newtonsoft.Json;

namespace CinemaAPI.Controllers
{
    public class BalanceTransactionsController : ApiController
    {
        private CinemaDBEntities db = new CinemaDBEntities();

        // GET: api/BalanceTransactions
        public IQueryable<BalanceTransaction> GetBalanceTransactions()
        {
            return db.BalanceTransactions;
        }

        // GET: api/BalanceTransactions/5
        [ResponseType(typeof(BalanceTransaction))]
        public IHttpActionResult GetBalanceTransaction(int id)
        {
            BalanceTransaction balanceTransaction = db.BalanceTransactions.Find(id);
            if (balanceTransaction == null)
            {
                return NotFound();
            }

            return Ok(balanceTransaction);
        }

        [HttpPost]
        [Route("api/BalanceTransactions/GetTransactionHistory")]
        public IHttpActionResult getTransactionHistory()
        {
            HttpContent requestContent = Request.Content;
            String json = requestContent.ReadAsStringAsync().Result;
            var receiveModel = JsonConvert.DeserializeObject<ReceiveModel>(json);
            String userName = receiveModel.username;
            String listTransactionJson = "";
            bool isSuccess = false;
            if (userName != null)
            {
                try
                {
                    var userTransactionHistory = db.BalanceTransactions.Where(a => a.User == userName).ToList();
                    var listTransaction = userTransactionHistory.Select(u => new BalanceTransactionModel
                    {
                        CreateDate = u.CreatedDate.ToString() ,
                        OrderId = u.OrderId,
                        TotalAmount = Double.Parse(u.Amount.GetValueOrDefault().ToString()),
                    });
                    listTransactionJson = JsonConvert.SerializeObject(listTransaction);
                    db.SaveChanges();
                    isSuccess = true;
                }
                catch (Exception)
                {
                    isSuccess = false;
                }
            }

            return Json(new { isSuccess, listTransactionJson });
        }
        public class BalanceTransactionModel
        {
            public String CreateDate { get; set; }
            public Double TotalAmount { get; set; }
            public String OrderId { get; set; }
        }
        public class ReceiveModel {
            public string username { get; set; }
        }

    }
}