﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CinemaAPI.Models;
using Newtonsoft.Json;

namespace CinemaAPI.Controllers
{
    public class AccountsController : ApiController
    {
        private CinemaDBEntities db = new CinemaDBEntities();

        // GET: api/Accounts
        [HttpPost]
        [Route("api/Accounts/GetUserWalletPost")]
        public IHttpActionResult getUserWalletPost()
        {
            HttpContent requrestContent = Request.Content;
            String getJson = requrestContent.ReadAsStringAsync().Result;
            ReceiveModel recieveModel = JsonConvert.DeserializeObject<ReceiveModel>(getJson);
            var userWallet = Double.Parse(db.Accounts.Where(a => a.Username.Equals(recieveModel.username)).FirstOrDefault().Balance.ToString());
            return Json
                (
                new { userWallet }
                );
        }
        [HttpPost]
        [Route("api/Accounts/CheckUserLogin")]
        public IHttpActionResult checkUserLogin()
        {
            HttpContent requestContent = Request.Content;
            String json = requestContent.ReadAsStringAsync().Result;
            CheckUserModel userModel = JsonConvert.DeserializeObject<CheckUserModel>(json);
            var validUser = db.Accounts.Where(a => a.Username == userModel.Username && a.Password == userModel.Password);
            bool isSuccess;
            if (validUser != null)
            {
                try
                {
                    validUser.FirstOrDefault().Active = true;
                    db.SaveChanges();
                    isSuccess = true;

                }
                catch (Exception e)
                {
                    isSuccess = false;
                }
            }
            else
            {
                isSuccess = false;
            }
            return Json
                  (
                        new
                        {
                            isSuccess
                        }
                  );
        }


        [HttpPost]
        [Route("api/Accounts/CheckUserSignUp")]
        public IHttpActionResult checkUserSignUp()
        {
            HttpContent requestContent = Request.Content;
            String json = requestContent.ReadAsStringAsync().Result;
            SignUpModel signUpModel = JsonConvert.DeserializeObject<SignUpModel>(json);
            bool isSuccess;
            String message = "";
            if (signUpModel != null)
            {
                if (db.Accounts.Any(a => a.Username.Equals(signUpModel.Username)))
                {
                    isSuccess = false;
                    message = "Username exists";
                }
                else if (db.Accounts.Any(a => a.Email.Equals(signUpModel.Email)))
                {
                    isSuccess = false;
                    message = "Email exists";
                }
                else if (db.Accounts.Any(a => a.Phone.Equals(signUpModel.Phone)))
                {
                    isSuccess = false;
                    message = "Phone exists";
                }
                else
                {
                    try
                    {
                        db.Accounts.Add(new Account
                        {
                            Username = signUpModel.Username,
                            Password = signUpModel.Password,
                            Email = signUpModel.Email,
                            Address = signUpModel.Address,
                            Phone = signUpModel.Phone,
                            Balance = 0,
                            Active = false,
                            Role = 2,
                            StartDate = System.DateTime.Now
                        });
                        db.SaveChanges();
                        isSuccess = true;
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        message = "Can not create new user . Please contact admin for more info";
                    }
                }

            }
            else
                isSuccess = false;
            return Json(new
            {
                isSuccess,
                message
            });
        }

        [HttpPost]
        [Route("api/Accounts/GetAccountByUsername")]
        public IHttpActionResult GetAccountByUsername()
        {
            HttpContent requestContent = Request.Content;
            String json = requestContent.ReadAsStringAsync().Result;
            var receiveModel = JsonConvert.DeserializeObject<ReceiveModel>(json);
            String userName = receiveModel.username;
            var accountSelected = db.Accounts.Where(a => a.Username.Equals(userName)).Select(a => new AccountModel
            {
                Username = a.Username,
                Email = a.Email,
                Address = a.Address,
                Phone = a.Phone,
                Wallet = (Double)a.Balance,
            }).FirstOrDefault();
            String accountJson = JsonConvert.SerializeObject(accountSelected);
            return Json(new
            {
                accountJson
            });
        }
        public class ReceiveModel
        {
            public string username { get; set; }
        }
        public class SignUpModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Address { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }

        }
        public class AccountModel
        {
            public string Username { get; set; }
            public string Address { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public double Wallet { get; set; }
        }
        public class CheckUserModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }
        
    }
}